import unittest
#from unittest.mock import patch

from tsplib.factory import ParserFactory
from tsplib.reader import LineReader
from tsplib.parser import Descriptor, TypeSpec, EdgeWeightTypeSpec, \
    NodeCoordTypeSpec
from tsplib.path import absolute_path


class TestParser(unittest.TestCase):

    def setUp(self):
        self.descriptor = Descriptor()
        factory = ParserFactory()
        self.parser = factory.create(descriptor=self.descriptor)

    def tearDown(self):
        self.descriptor.reset()

    def parse_test_file(self, test_file):
        reader = LineReader(abc_reader_handler=self.parser)
        file_path = absolute_path(test_file, __file__)
        reader.read(file_path)

        return reader

    def test_uninitialized(self):
        self.assertEqual(self.descriptor.name, '')
        self.assertEqual(self.descriptor.type, None)
        self.assertEqual(self.descriptor.edge_weight_type, None)
        self.assertEqual(len(self.descriptor.get_comments()), 0)

    def test_parse_name(self):
        self.parse_test_file('assets/samp2.tsp')
        self.assertEqual(self.descriptor.name, 'samp2')

    def test_parse_comments(self):
        desc = self.descriptor
        self.parse_test_file('assets/samp2.tsp')
        self.assertEqual(len(desc.get_comments()), 5)
        self.assertEqual(desc.get_comment(0), '2 locations in samp2')
        self.assertEqual(desc.get_comment(4), 'requesting data sets without '
                                              'duplications.')

    def test_parse_type(self):
        self.parse_test_file('assets/samp2.tsp')
        self.assertEqual(self.descriptor.type, TypeSpec.TSP)

    def test_parse_edge_weight_type(self):
        self.parse_test_file('assets/samp2.tsp')
        self.assertEqual(self.descriptor.edge_weight_type,
                         EdgeWeightTypeSpec.EUC_2D)

    def test_parse_dimension(self):
        self.parse_test_file('assets/samp2.tsp')
        self.assertEqual(self.descriptor.dimension, 5)

    def test_parse_capacity(self):
        self.parse_test_file('assets/samp2.tsp')
        self.assertEqual(self.descriptor.capacity, 0)

    def test_parse_edge_weight_format(self):
        self.parse_test_file('assets/samp2.tsp')
        self.assertEqual(self.descriptor.edge_weight_format, None)

    def test_parse_edge_data_format(self):
        self.parse_test_file('assets/samp2.tsp')
        self.assertEqual(self.descriptor.edge_data_format, None)

    def test_parse_display_data_type(self):
        self.parse_test_file('assets/samp2.tsp')
        self.assertEqual(self.descriptor.display_data_type, None)

    def test_parse_node_coord_type(self):
        self.parse_test_file('assets/samp2.tsp')
        self.assertEqual(
            self.descriptor.node_coord_type,
            NodeCoordTypeSpec.TWOD_COORDS
        )

    def test_parse_node_coords(self):
        self.parse_test_file('assets/samp2.tsp')
        self.assertEqual(len(self.descriptor.vertices), 5)
        self.assertEqual(
            self.descriptor.vertices[0]['vertex'][0],
            11003.611100
        )
        self.assertEqual(
            self.descriptor.vertices[4]['vertex'][1],
            42933.333300
        )


if __name__ == '__main__':
    unittest.main()
