import unittest

from copy import copy

from tsplib.mutation.mutators import Shifting, OrOpt, TwoExchange, Lin2Opt
from tsplib.mutation.validators import MutationValidator


class MutatorsTestCase(unittest.TestCase):
    def test_shifting(self):
        tour = [1, 2, 3, 4, 5, 6, 7, 8]
        new_tour = copy(tour)
        op = Shifting(MutationValidator())

        for t in range(100):
            new_tour = op.mutate(new_tour)
            self.assertEqual(len(new_tour), len(tour))

            compare = copy(tour)
            for c in new_tour:
                compare.remove(c)

            self.assertEqual(len(compare), 0)

    def test_or_opt(self):
        tour = [1, 2, 3, 4, 5, 6, 7, 8]
        new_tour = copy(tour)
        op = OrOpt(MutationValidator())

        for t in range(100):
            new_tour = op.mutate(new_tour)
            self.assertEqual(len(new_tour), len(tour))

            compare = copy(tour)
            for c in new_tour:
                compare.remove(c)

            self.assertEqual(len(compare), 0)

    def test_two_exchange(self):
        tour = [1, 2, 3, 4, 5, 6, 7, 8]
        new_tour = copy(tour)
        op = TwoExchange(MutationValidator())

        for t in range(100):
            new_tour = op.mutate(new_tour)
            self.assertEqual(len(new_tour), len(tour))

            compare = copy(tour)
            for c in new_tour:
                compare.remove(c)

            self.assertEqual(len(compare), 0)

    def test_lin2opt(self):
        tour = [1, 2, 3, 4, 5, 6, 7, 8]
        new_tour = copy(tour)
        op = Lin2Opt(MutationValidator())

        for t in range(100):
            new_tour = op.mutate(new_tour)
            self.assertEqual(len(new_tour), len(tour))

            compare = copy(tour)
            for c in new_tour:
                compare.remove(c)

            self.assertEqual(len(compare), 0)


if __name__ == '__main__':
    unittest.main()
