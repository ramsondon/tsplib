import unittest

from tsplib.algorithm.hill_climbing import HillClimbing
from tsplib.distance import TourDistance, EuclideanDistance2D, \
    DictVertexDataMapper
from tsplib.mutation.mutators import Lin2Opt, BruteForce
from tsplib.mutation.validators import MatrixMutationValidator


class HillClimbingTestCase(unittest.TestCase):

    def test_symmetric_20(self):
        cities = [
            {'id': 1, 'vertex': [60, 200]},
            {'id': 2, 'vertex': [180, 200]},
            {'id': 3, 'vertex': [80, 180]},
            {'id': 4, 'vertex': [140, 180]},
            {'id': 5, 'vertex': [20, 160]},
            {'id': 6, 'vertex': [100, 160]},
            {'id': 7, 'vertex': [200, 160]},
            {'id': 8, 'vertex': [140, 140]},
            {'id': 9, 'vertex': [40, 120]},
            {'id': 10, 'vertex': [100, 120]},
            {'id': 11, 'vertex': [180, 100]},
            {'id': 12, 'vertex': [60, 80]},
            {'id': 13, 'vertex': [120, 80]},
            {'id': 14, 'vertex': [180, 60]},
            {'id': 15, 'vertex': [20, 40]},
            {'id': 16, 'vertex': [100, 40]},
            {'id': 17, 'vertex': [200, 40]},
            {'id': 18, 'vertex': [20, 20]},
            {'id': 19, 'vertex': [60, 20]},
            {'id': 20, 'vertex': [160, 20]},
        ]

        mapper = DictVertexDataMapper('vertex')

        # dist = ManhattanDistance2D(mapper=mapper, dimension=len(cities),
        # vertices=cities)
        # dist = MaximumDistance2D(mapper=mapper, dimension=len(cities),
        # vertices=cities)

        dist = EuclideanDistance2D(mapper=mapper, dimension=len(cities),
                                   vertices=cities)
        matrix = dist.to_matrix()
        td = TourDistance(distance_matrix=matrix)
        validator = MatrixMutationValidator(matrix)
        op = Lin2Opt(validator)
        op = BruteForce(validator)
        #op = Shifting(validator)
        #op = OrOpt(validator)
        #op = TwoExchange(validator)

        algo = HillClimbing(mutator=op, distance=td)

        result = algo.run(y_p=dist.to_vector())
        print('HillClimbing:', result)


if __name__ == '__main__':
    unittest.main()
