import unittest
from tsplib.algorithm.simulated_annealing import SimulatedAnnealing
from copy import copy

from tsplib.distance import EuclideanDistance2D, ManhattanDistance2D, \
    MaximumDistance2D, DictVertexDataMapper, MatrixDistance, TourDistance
from tsplib.mutation.mutators import Shifting, OrOpt, TwoExchange, Lin2Opt
from tsplib.mutation.validators import MutationValidator, \
    MatrixMutationValidator


class SimulatedAnnealingTestCase(unittest.TestCase):

    def test_symmetric_20(self):

        cities = [
            {'id': 1, 'vertex': [60, 200]},
            {'id': 2, 'vertex': [180, 200]},
            {'id': 3, 'vertex': [80, 180]},
            {'id': 4, 'vertex': [140, 180]},
            {'id': 5, 'vertex': [20, 160]},
            {'id': 6, 'vertex': [100, 160]},
            {'id': 7, 'vertex': [200, 160]},
            {'id': 8, 'vertex': [140, 140]},
            {'id': 9, 'vertex': [40, 120]},
            {'id': 10, 'vertex': [100, 120]},
            {'id': 11, 'vertex': [180, 100]},
            {'id': 12, 'vertex': [60, 80]},
            {'id': 13, 'vertex': [120, 80]},
            {'id': 14, 'vertex': [180, 60]},
            {'id': 15, 'vertex': [20, 40]},
            {'id': 16, 'vertex': [100, 40]},
            {'id': 17, 'vertex': [200, 40]},
            {'id': 18, 'vertex': [20, 20]},
            {'id': 19, 'vertex': [60, 20]},
            {'id': 20, 'vertex': [160, 20]},
        ]

        mapper = DictVertexDataMapper('vertex')


        # dist = ManhattanDistance2D(mapper=mapper, dimension=len(cities),
        # vertices=cities)
        # dist = MaximumDistance2D(mapper=mapper, dimension=len(cities),
        # vertices=cities)

        dist = EuclideanDistance2D(mapper=mapper, dimension=len(cities),
                                   vertices=cities)
        matrix = dist.to_matrix()
        td = TourDistance(distance_matrix=matrix)
        validator = MatrixMutationValidator(matrix)
        op = Lin2Opt(validator)
        #op = Shifting(validator)
        #op = OrOpt(validator)
        #op = TwoExchange(validator)

        algo = SimulatedAnnealing(mutator=op, distance=td)

        result = algo.run(y_p=dist.to_vector())
        print('Simulated Annealing:', result)


if __name__ == '__main__':
    unittest.main()
