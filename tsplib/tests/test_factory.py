import unittest

from tsplib.distance import EuclideanDistance3D, EuclideanDistance2D, \
    ManhattanDistance2D, ManhattanDistance3D, MaximumDistance2D, \
    MaximumDistance3D, MatrixDistance
from tsplib.factory import DistanceFunctionFactory
from tsplib.parser import Descriptor, EdgeWeightTypeSpec, EdgeWeightFormatSpec


class FactoryTestCase(unittest.TestCase):

    def setUp(self):
        self.descriptor = Descriptor()
        self.descriptor.edge_weight_type = EdgeWeightTypeSpec.EUC_2D
        self.factory = DistanceFunctionFactory()

    def test_distance_factory_explicit(self):
        self.descriptor.edge_weight_type = EdgeWeightTypeSpec.EXPLICIT
        self.descriptor.edge_weight_format = EdgeWeightFormatSpec.FULL_MATRIX
        df = self.factory.create(self.descriptor)
        self.assertTrue(isinstance(df, MatrixDistance))
        #self.assertRaises(TypeError, self.factory.create, self.descriptor)

    def test_distance_euclidean_2d(self):
        self.descriptor.edge_weight_type = EdgeWeightTypeSpec.EUC_2D
        df = self.factory.create(self.descriptor)
        self.assertTrue(isinstance(df, EuclideanDistance2D))

    def test_distance_euclidean_3d(self):
        self.descriptor.edge_weight_type = EdgeWeightTypeSpec.EUC_3D
        df = self.factory.create(self.descriptor)
        self.assertTrue(isinstance(df, EuclideanDistance3D))

    def test_distance_manhattan_2d(self):
        self.descriptor.edge_weight_type = EdgeWeightTypeSpec.MAN_2D
        df = self.factory.create(self.descriptor)
        self.assertTrue(isinstance(df, ManhattanDistance2D))

    def test_distance_manhattan_3d(self):
        self.descriptor.edge_weight_type = EdgeWeightTypeSpec.MAN_3D
        df = self.factory.create(self.descriptor)
        self.assertTrue(isinstance(df, ManhattanDistance3D))

    def test_distance_maximum_2d(self):
        self.descriptor.edge_weight_type = EdgeWeightTypeSpec.MAX_2D
        df = self.factory.create(self.descriptor)
        self.assertTrue(isinstance(df, MaximumDistance2D))

    def test_distance_maximum_3d(self):
        self.descriptor.edge_weight_type = EdgeWeightTypeSpec.MAX_3D
        df = self.factory.create(self.descriptor)
        self.assertTrue(isinstance(df, MaximumDistance3D))


if __name__ == '__main__':
    unittest.main()
