import unittest

from tsplib.algorithm.simulated_annealing import SimulatedAnnealing
from tsplib.distance import TourDistance
from tsplib.factory import ParserFactory, DistanceFunctionFactory
from tsplib.mutation.mutators import Lin2Opt, TwoExchange, OrOpt, Shifting
from tsplib.mutation.validators import MatrixMutationValidator
from tsplib.parser import Descriptor
from tsplib.path import absolute_path
from tsplib.reader import LineReader


class TestTSPLib(unittest.TestCase):

    name = 'burma14.tsp'
    #name = 'att48.tsp'
    #name = 'br17.atsp'
    #name = 'dj38.tsp'
    #name = 'p43.atsp'
    #name = 'ftv33.atsp'
    #name = 'ft53.atsp'
    #name = 'ftv170.atsp'
    #name = 'gr666.tsp'

    def test_sim_annealing(self):

        descriptor = Descriptor()
        pf = ParserFactory()
        parser = pf.create(descriptor=descriptor)

        reader = LineReader(abc_reader_handler=parser)
        name = self.name
        ext = name.partition('.')[2]
        file_path = absolute_path(f'../../assets/{ext}/tests/{name}', __file__)
        reader.read(file_path)

        df = DistanceFunctionFactory()
        dd = df.create(descriptor=descriptor)
        matrix = dd.to_matrix()
        validator = MatrixMutationValidator(matrix)

        #operator = TwoExchange(validator=validator)
        #operator = OrOpt(validator=validator)
        #operator = Shifting(validator=validator)
        operator = Lin2Opt(validator=validator)

        algo = SimulatedAnnealing(
            mutator=operator, distance=TourDistance(distance_matrix=matrix))
        algo.t_init = 100
        algo.alpha = 0.7
        algo.t_const_gen = algo.t_init * 0.1

        result = algo.run(y_p=dd.to_vector())
        print('Simulated Annealing:', result)


if __name__ == '__main__':
    unittest.main()
