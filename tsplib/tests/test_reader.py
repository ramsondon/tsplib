import unittest
from tsplib.reader import LineReader, LineCounter
from tsplib.path import absolute_path


class TestReader(unittest.TestCase):

    def setUp(self):
        self.nr_of_lines = 16
        counter_handler = LineCounter()
        self.reader = LineReader(counter_handler)

    def read_file(self):
        file_path = absolute_path('assets/samp2.tsp', __file__)
        self.reader.read(file_path)

    def test_count_lines(self):
        self.assertEqual(self.reader.handler.count(), 0)
        self.read_file()
        self.assertEqual(self.reader.handler.count(), self.nr_of_lines)

    def test_add_line_counter_handler(self):
        self.reader.register_handler('count', LineCounter())
        self.read_file()
        self.assertEqual(self.reader.get_handler('count').count(),
                         self.nr_of_lines)


if __name__ == '__main__':
    unittest.main()
