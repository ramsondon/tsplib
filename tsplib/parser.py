from abc import ABCMeta, abstractmethod
from math import floor

from tsplib.reader import ABCReaderHandler
from enum import Enum

# all constants come from the tsplib specification available as pdf in
# /doc/tsp95.pdf


class Section(Enum):
    """
    a file consists of two parts. spec and data part
    """
    SPEC = 1

    NODE_COORD_SECTION = 2
    DEPOT_SECTION = 3
    DEMAND_SECTION = 4
    EDGE_DATA_SECTION = 5
    FIXED_EDGES_SECTION = 6
    DISPLAY_DATA_SECTION = 7
    TOUR_SECTION = 8
    EDGE_WEIGHT_SECTION = 9


class Spec(Enum):
    """
    main specification for tsplib95
    """
    NAME = 1
    TYPE = 2
    COMMENT = 3
    DIMENSION = 4
    CAPACITY = 5
    EDGE_WEIGHT_TYPE = 6
    EDGE_WEIGHT_FORMAT = 7
    EDGE_DATA_FORMAT = 8
    NODE_COORD_TYPE = 9
    DISPLAY_DATA_TYPE = 10
    EOF = 11

    NODE_COORD_SECTION = 12
    DEPOT_SECTION = 13
    DEMAND_SECTION = 14
    EDGE_DATA_SECTION = 15
    FIXED_EDGES_SECTION = 16
    DISPLAY_DATA_SECTION = 17
    TOUR_SECTION = 18
    EDGE_WEIGHT_SECTION = 19


class TypeSpec(Enum):
    TSP = 1
    ATSP = 2
    SOP = 3
    HCP = 4
    CVRP = 5
    TOUR = 6


class EdgeWeightTypeSpec(Enum):
    EXPLICIT = 1
    EUC_2D = 2
    EUC_3D = 3
    MAX_2D = 4
    MAX_3D = 5
    MAN_2D = 6
    MAN_3D = 7
    CEIL_2D = 8
    GEO = 9
    ATT = 10
    XRAY1 = 11
    XRAY2 = 12
    SPECIAL = 13


class EdgeWeightFormatSpec(Enum):
    FUNCTION = 1
    FULL_MATRIX = 2
    UPPER_ROW = 3
    LOWER_ROW = 4
    UPPER_DIAG_ROW = 5
    LOWER_DIAG_ROW = 6
    UPPER_COIL = 7
    LOWER_COIL = 8
    UPPER_DIAG_COL = 9
    LOWER_DIAG_COL = 10


class EdgeDataFormatSpec(Enum):
    EDGE_LIST = 1
    ADJ_LIST = 2


class NodeCoordTypeSpec(Enum):
    """
    Specifies whether coordinates are associated with each node (which,
    for example may be used for either graphical display or distance
    computations).
    """
    TWOD_COORDS = 1
    THREED_COORDS = 2
    NO_COORDS = 3


class DisplayDataTypeSpec(Enum):
    COORD_DISPLAY = 1
    TWOD_DISPLAY = 2
    NO_DISPLAY = 3


class FileFormatException(Exception):
    pass


class Descriptor:
    def __init__(self):
        self.name = ''
        self.__comments = []
        self.type = None
        self.dimension = 0
        self.capacity = 0
        self.edge_weight_type = None
        self.edge_weight_format = None
        self.edge_data_format = None
        self.display_data_type = None
        self.node_coord_type = None
        self.vertices = []

    def get_comments(self):
        return self.__comments

    def get_comment(self, idx):
        return self.__comments[idx]

    def add_comment(self, comment):
        self.__comments.append(comment)

    def reset(self):
        self.name = ''
        self.__comments = []
        self.type = None
        self.dimension = 0
        self.capacity = 0
        self.edge_weight_type = None
        self.edge_weight_format = None
        self.edge_data_format = None
        self.display_data_type = None
        self.node_coord_type = None


class SpecHandler(metaclass=ABCMeta):
    def __init__(self, descriptor):
        self.descriptor = descriptor

    @abstractmethod
    def parse(self, string):
        pass

    def raise_type_error(self, string):
        raise TypeError("{} is not a valid type".format(string))


class NameSpecHandler(SpecHandler):
    def parse(self, string):
        self.descriptor.name = string


class CommentSpecHandler(SpecHandler):
    def parse(self, string):
        self.descriptor.add_comment(string)


class TypeSpecHandler(SpecHandler):
    def parse(self, string):
        for t in TypeSpec:
            if t.name.upper() == string.upper():
                self.descriptor.type = t
                return

        self.raise_type_error(string)


class DimensionSpecHandler(SpecHandler):
    def parse(self, string):
        self.descriptor.dimension = int(string)


class CapacitySpecHandler(SpecHandler):
    def parse(self, string):
        self.descriptor.capacity = int(string)


class EdgeWeightTypeSpecHandler(SpecHandler):
    def parse(self, string):
        for t in EdgeWeightTypeSpec:
            if t.name.upper() == string.upper():
                self.descriptor.edge_weight_type = t
                return

        self.raise_type_error(string)


class EdgeWeightFormatSpecHandler(SpecHandler):
    def parse(self, string):
        for t in EdgeWeightFormatSpec:
            if t.name.upper() == string.upper():
                self.descriptor.edge_weight_format = t
                return

        self.raise_type_error(string)


class EdgeDataFormatSpecHandler(SpecHandler):
    def parse(self, string):
        for t in EdgeDataFormatSpec:
            if t.name.upper() == string.upper():
                self.descriptor.edge_data_format = t
                return

        self.raise_type_error(string)


class DisplayDataTypeSpecHandler(SpecHandler):
    def parse(self, string):
        for t in DisplayDataTypeSpec:
            if t.name.upper() == string.upper():
                self.descriptor.display_data_type = t
                return

        self.raise_type_error(string)


class NodeCoordTypeSpecHandler(SpecHandler):
    def parse(self, string):
        for t in NodeCoordTypeSpec:
            if t.name.upper() == string.upper():
                self.descriptor.node_coord_type = t
                return

        self.raise_type_error(string)


class Parser(ABCReaderHandler):
    def __init__(self, descriptor):
        super().__init__()
        self.separator_spec = ':'
        self.separator_data = ' '
        self.sections = {}
        self.current_section = None
        self.__descriptor = descriptor
        self.__data_handlers = {}

    def register_section_handler(self, key, handler):
        self.sections[key.upper()] = handler

    def get_default_section_handler(self):
        default_key = next(iter(self.sections))
        return self.sections[default_key]

    def get_section_handler(self, content):

        handler = self.sections.get(content.strip().upper(), None)
        if None is handler and None is self.current_section:
            self.current_section = self.get_default_section_handler()
        elif None is not handler:
            self.current_section = handler

        return self.current_section

    def handle(self, content, prev_handler=None):
        if content.strip().upper() != Spec.EOF.name.upper():
            super().handle(content, prev_handler)

    def _do_handle(self, content, prev_handler=None):

        if len(self.sections.keys()) <= 0:
            raise ValueError('Parser cannot parse without any registered '
                             'sections')

        handler = self.get_section_handler(content)
        return handler.parse(content)


class SectionHandler(metaclass=ABCMeta):
    def __init__(self, descriptor):
        self.descriptor = descriptor

    @abstractmethod
    def parse(self, content):
        pass


class SectionSpec(SectionHandler):
    def __init__(self, descriptor):
        super().__init__(descriptor)
        self.separator = ':'
        self.parser = None
        self.__spec_handlers = {}

    def register_spec_handler(self, spec, handler):
        self.__spec_handlers[spec.name.upper()] = handler

    def parse(self, content):
        triple = content.partition(self.separator)
        spec_name = triple[0].strip().upper()
        data = triple[2].strip()

        try:
            spec_handler = self.__spec_handlers[spec_name]
            return spec_handler.parse(data)
        except KeyError:
            return data


class SectionNodeCoord(SectionHandler):
    def __init__(self, descriptor):
        super().__init__(descriptor)
        self.separator = ' '
        self.first_call = True

    def parse(self, content):

        if True is self.first_call:
            self.first_call = False
            return

        params = content.strip().split(self.separator)

        # remove unnecessary empty entries in our array
        params = ' '.join(params).split()

        node_coord_type = self.descriptor.node_coord_type
        coords = []

        if len(params) == 3 \
                and node_coord_type == NodeCoordTypeSpec.TWOD_COORDS:
            x = float(params[1].strip())
            y = float(params[2].strip())
            coords = [x, y]

        elif len(params) == 4 \
                and node_coord_type == NodeCoordTypeSpec.THREED_COORDS:
            x = float(params[1].strip())
            y = float(params[2].strip())
            z = float(params[3].strip())
            coords = [x, y, z]

        if node_coord_type is not NodeCoordTypeSpec.NO_COORDS \
                and len(coords) > 0:
            self.descriptor.vertices.append({
                'id': int(params[0].strip()),
                'vertex': coords
            })

        else:
            raise FileFormatException("incorrect NODE_COORD_TYPE for "
                                      "NODE_COORD_SECTION")


class SectionEdgeWeight(SectionHandler):
    def __init__(self, descriptor):
        super().__init__(descriptor)
        self.separator = ' '
        self.first_call = True
        self.matrix = [[]]
        self.cursor = 0
        self.max_cells = 0

    def parse(self, content):

        d = self.descriptor

        if True is self.first_call:
            self.first_call = False
            self.matrix = [
                [0 for i in range(d.dimension)]
                for k in range(d.dimension)
            ]
            self.max_cells = d.dimension**2
            return

        if d.edge_weight_format == EdgeWeightFormatSpec.FULL_MATRIX:
            params = content.strip().split(self.separator)

            # remove unnecessary empty entries in our array
            params = ' '.join(params).split()

            for i in range(len(params)):
                x = floor(self.cursor / d.dimension)
                y = self.cursor % d.dimension

                self.matrix[x][y] = params[i]
                self.cursor += 1

            if self.cursor >= self.max_cells:
                d.vertices = self.matrix
