from abc import ABCMeta, abstractmethod
from math import floor
from random import random

from copy import copy

from tsplib.mutation.exceptions import InvalidTourException
from tsplib.mutation.validators import MutationValidator


class MutationOperator(metaclass=ABCMeta):

    def __init__(self, validator: MutationValidator):
        self.validator = validator
        self.max_generations_while_zero = 1000
        self.mutate_while_invalid = True
        self.operation_count = 0

    def mutate(self, y_p):
        c = 1
        y_p = self._mutate(y_p)

        while True is self.mutate_while_invalid \
                and c < self.max_generations_while_zero \
                and not self.validator.validate(y_p):

            y_p = self._mutate(y_p)
            c += 1

        if True is self.mutate_while_invalid \
                and c >= self.max_generations_while_zero \
                and not self.validator.validate(y_p):

            raise InvalidTourException('maximum re-mutation tries exceeded')

        self.operation_count += c

        return y_p

    @abstractmethod
    def _mutate(self, y_p):
        return y_p

    def random(self, np, nr=1):
        rands = []
        for i in range(0, nr):
            rands.append(floor(random() * np + 1))
        return list(rands)

    def get_vars(self, y_p):
        np = len(y_p)
        (s, e) = self.random(np, 2)

        while s + 1 > e:
            (s, e) = self.random(np, 2)

        return np, s, e


class Shifting(MutationOperator):
    def _mutate(self, y_p):
        np = len(y_p)
        (s, m, e) = self.random(np, 3)

        while s >= m or m >= e:
            (s, m, e) = self.random(np, 3)

        return y_p[0:s] + y_p[m:e] + y_p[s:m] + y_p[e:np]


class OrOpt(MutationOperator):
    def _mutate(self, y_p):
        (np, s, e) = self.get_vars(y_p)
        return y_p[0:s] + y_p[s+1:e] + y_p[s:s+1] + y_p[e:np]


class TwoExchange(MutationOperator):
    def _mutate(self, y_p):
        (np, s, e) = self.get_vars(y_p)
        return y_p[0:s] + y_p[e:e+1] + y_p[s+1:e] + y_p[s:s+1] + y_p[e+1:np]


class Lin2Opt(MutationOperator):
    def _mutate(self, y_p):
        (np, s, e) = self.get_vars(y_p)
        return y_p[0:s] + y_p[s:e][::-1] + y_p[e:np]


class BruteForce(MutationOperator):
    """
    customized implementation of B. R. Heap
    from: https://en.wikipedia.org/wiki/Heap%27s_algorithm
    """

    def __init__(self, validator: MutationValidator):
        super().__init__(validator=validator)
        self.i = 0
        self.c = None

    def _mutate(self, y_p):
        y = copy(y_p)
        n = len(y)

        if None is self.c:
            self.c = [0 for i in range(n)]

        while self.i < n:

            if self.c[self.i] < self.i:

                # if i is even then swap(A[0], A[i])
                if self.i % 2 is 0:
                    y[self.i], y[0] = y[0], y[self.i]
                else:
                    y[self.c[self.i]], y[self.i] = y[self.i], y[self.c[self.i]]

                self.c[self.i] += 1
                self.i = 0

                return y
            else:
                self.c[self.i] = 0
                self.i += 1

        return y
