
class MutationValidator:
    def validate(self, y):
        return True


class MatrixMutationValidator(MutationValidator):
    def __init__(self, distance_matrix):
        self.matrix = distance_matrix

    def validate(self, y):
        for i in range(0, len(y) - 2):
            val = self.matrix[y[i]][y[i + 1]]
            if val <= 0:
                return False

        return self.matrix[y[len(y) - 1]][y[0]] > 0

