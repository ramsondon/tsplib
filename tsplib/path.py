import os


def absolute_path(path, from_path):
    real_path = os.path.realpath(from_path)
    directory = os.path.dirname(real_path)

    return "{}/{}".format(directory, path.lstrip('/'))
