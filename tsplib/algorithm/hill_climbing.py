from tsplib.algorithm.algorithm import Algorithm, Result
from tsplib.distance import TourDistance
from tsplib.mutation.mutators import MutationOperator


class HillClimbing(Algorithm):
    def __init__(self, mutator: MutationOperator, distance: TourDistance):
        super().__init__(mutator=mutator, distance=distance)
        self.max_generations = 3000

    def run(self, y_p):

        if not self.mutator.validator.validate(y_p):
            y_p = self.mutator.mutate(y_p)

        f_p = self.distance.distance(y_p)

        g = 0
        while g < self.max_generations:
            y_c = self.mutator.mutate(y_p)
            f_c = self.distance.distance(y_c)

            if f_c < f_p:
                y_p = y_c
                f_p = f_c

            g += 1

        return Result(distance=f_p, generations=g,
                      mutations=self.mutator.operation_count, tour=y_p)
