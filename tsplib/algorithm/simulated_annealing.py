from math import exp
from random import random

from tsplib.algorithm.algorithm import Algorithm, Result
from tsplib.distance import TourDistance
from tsplib.mutation.mutators import MutationOperator


class SimulatedAnnealing(Algorithm):
    def __init__(self, mutator: MutationOperator, distance: TourDistance):
        super().__init__(mutator=mutator, distance=distance)

        # algorithm parameters
        self.t_init = 100
        self.t_stop = 1e-30
        self.alpha = 0.7

        # nr of generations of a constant temperature
        self.t_const_gen = self.t_init * 0.1

    def run(self, y_p):
        """
        :param y_p: init vector
        :return: object
        """
        if not self.mutator.validator.validate(y_p):
            y_p = self.mutator.mutate(y_p)

        f_p = self.distance.distance(y_p)
        t = self.t_init
        g = 0

        while t >= self.t_stop:
            # create new Mutation
            y_c = self.mutator.mutate(y_p)
            # calculate tsp distance
            f_c = self.distance.distance(y_c)

            # accept new route if distance is smaller than before or through
            # temperature
            if f_c <= f_p or exp((f_p - f_c) / t) > random():
                y_p = y_c
                f_p = f_c

            # cool temperature if past constant period
            if g % self.t_const_gen == self.t_const_gen - 1:
                t *= self.alpha

            # increment generation counter
            g += 1

        return Result(distance=f_p, generations=g,
                      mutations=self.mutator.operation_count, tour=y_p)
