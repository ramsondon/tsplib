from abc import ABCMeta, abstractmethod

from tsplib.distance import TourDistance
from tsplib.mutation.mutators import MutationOperator


class Algorithm(metaclass=ABCMeta):
    def __init__(self, mutator: MutationOperator, distance: TourDistance):
        self.distance = distance
        self.mutator = mutator

    @abstractmethod
    def run(self, y_p):
        pass


class Result:
    def __init__(self, distance, tour, generations, mutations):
        self.distance = distance
        self.tour = tour
        self.total_generations = generations
        self.total_mutations = mutations

    def get_nr_of_invalid_mutations(self):
        return self.total_mutations - self.total_generations

    def __str__(self):
        d = self.distance
        g = self.total_generations
        m = self.total_mutations

        return f"distance: {d}; generations: {g}; mutations: {m}"
