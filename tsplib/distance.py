from abc import ABCMeta, abstractmethod
from math import sqrt, pi, cos, acos


class TourDistance:
    def __init__(self, distance_matrix):
        self.matrix = distance_matrix

    def distance(self, y):
        d = 0
        for i in range(0, len(y) - 2):
            d += self.matrix[y[i]][y[i + 1]]

        d += self.matrix[y[len(y) - 1]][y[0]]

        return d


class VertexDataMapper:
    def map(self, node):
        return node


class DictVertexDataMapper(VertexDataMapper):
    def __init__(self, key: str):
        self.key = key

    def map(self, node):
        return node[self.key]


class DistanceFunction(metaclass=ABCMeta):

    def __init__(self, mapper: VertexDataMapper, dimension, vertices):
        self.mapper = mapper
        self.vertices = vertices
        self.dimension = dimension

    @abstractmethod
    def calculate(self, a, b):
        pass

    def nint(self, value):
        """
        round method proposed in documentation of tsplib /doc/tsp95.pdf
        :param value: float
        :return: int
        """
        return int(float(value) + 0.5)

    def to_vector(self):
        return [i for i in range(self.dimension)]

    def to_matrix(self):
        # mapping coordinates for FUNCTION
        dim = range(self.dimension)
        dd = [[0 for i in dim] for j in dim]
        v_len = len(self.vertices)
        for i in dim:
            for j in dim:
                if v_len > i and v_len > j:
                    a = self.mapper.map(self.vertices[i])
                    b = self.mapper.map(self.vertices[j])
                    dd[i][j] = self.calculate(a, b)

        return dd


class MatrixDistance(DistanceFunction):

    def calculate(self, a, b):
        pass

    def to_matrix(self):
        dim = range(self.dimension)
        dd = [[0 for i in dim] for k in dim]
        v_len = len(self.vertices)
        for i in dim:
            for j in dim:
                if v_len > i and len(self.vertices[i]) > j:
                    dd[i][j] = self.mapper.map(self.nint(self.vertices[i][j]))
                else:
                    dd[i][j] = 0
        return dd


class EuclideanDistance2D(DistanceFunction):
    def calculate(self, a, b):
        """
        implementation as proposed in spec of /doc/tsp95.pdf
        :param a: (x[i], y[i])
        :param b: (x[j], y[j])
        :return: int
        """

        xd = a[0] - b[0]
        yd = a[1] - b[1]

        return self.nint(sqrt(xd**2 + yd**2))


class EuclideanDistance3D(DistanceFunction):
    def calculate(self, a, b):
        """
        implementation as proposed in spec of /doc/tsp95.pdf
        :param a: (x[i], y[i], z[i])
        :param b: (x[j], y[j], z[j])
        :return: int
        """
        xd = a[0] - b[0]
        yd = a[1] - b[1]
        zd = a[2] - b[2]

        return self.nint(sqrt(xd**2 + yd**2 + zd**2))


class MaximumDistance2D(DistanceFunction):
    def calculate(self, a, b):
        """
        :param a: (x[i], y[i])
        :param b: (x[j], y[j])
        :return: int
        """
        xd = abs(a[0] - b[0])
        yd = abs(a[1] - b[1])

        return max(self.nint(xd), self.nint(yd))


class MaximumDistance3D(DistanceFunction):
    def calculate(self, a, b):
        """
        :param a: (x[i], y[i], z[i])
        :param b: (x[j], y[j], z[j])
        :return: int
        """
        xd = abs(a[0] - b[0])
        yd = abs(a[1] - b[1])
        zd = abs(a[2] - b[2])

        return max(self.nint(xd), self.nint(yd), self.nint(zd))


class ManhattanDistance2D(DistanceFunction):
    def calculate(self, a, b):
        """
        :param a: (x[i], y[i])
        :param b: (x[j], y[j])
        :return: int
        """
        xd = abs(a[0] - b[0])
        yd = abs(a[1] - b[1])

        return self.nint(xd + yd)


class ManhattanDistance3D(DistanceFunction):
    def calculate(self, a, b):
        """
        :param a: (x[i], y[i], z[i])
        :param b: (x[j], y[j], z[j])
        :return: int
        """
        xd = abs(a[0] - b[0])
        yd = abs(a[1] - b[1])
        zd = abs(a[2] - b[2])

        return self.nint(xd + yd + zd)


class GeoGraphicalDistance(DistanceFunction):
    EARTH_RADIUS = 6378.388

    def calculate(self, a, b):
        """
        calculates the geographical distance lng, lat
        :param a: (x[i], y[i])
        :param b: (x[j], y[j])
        :return: int
        """
        lat_0 = self.rad(a[0])
        lng_0 = self.rad(a[1])
        lat_1 = self.rad(b[0])
        lng_1 = self.rad(b[1])

        q1 = cos(lng_0 - lng_1)
        q2 = cos(lng_0 - lng_1)
        q3 = cos(lat_0 + lat_1)

        d = self.EARTH_RADIUS * acos(0.5*((1.0+q1)*q2 - (1.0-q1)*q3)) + 1.0
        return int(d)

    def rad(self, value):
        deg = self.nint(value)
        minimum = value - deg

        return pi * (deg + 5.0 * minimum / 3.0) / 180.0


class PseudoEuclideanDistance(DistanceFunction):
    def calculate(self, a, b):
        """
        :param a: (x[i], y[i])
        :param b: (x[j], y[j])
        :return: int
        """
        xd = a[0] - b[0]
        yd = a[1] - b[1]

        rij = sqrt((xd**2 + yd**2) / 10.0)
        tij = self.nint(rij)

        if tij < rij:
            return tij + 1
        else:
            return tij
