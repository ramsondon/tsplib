from abc import ABCMeta, abstractclassmethod


class ABCReader(metaclass=ABCMeta):

    @abstractclassmethod
    def read(self, file_path):
        pass


class ABCReaderHandler(metaclass=ABCMeta):
    def __init__(self):
        self.result = None

    @abstractclassmethod
    def _do_handle(self, content, prev_handler= None):
        pass

    def handle(self, content, prev_handler=None):
        self.result = self._do_handle(content, prev_handler)

    def on_finish(self):
        pass

    def get_result(self):
        return self.result


class LineCounter(ABCReaderHandler):
    def __init__(self):
        super().__init__()
        self.counter = 0

    def _do_handle(self, content, prev_handler=None):
        self.counter += 1
        return self.count()

    def count(self):
        return self.counter


class LineReader(ABCReader):
    def __init__(self, abc_reader_handler: ABCReaderHandler=None):
        self.handler = abc_reader_handler
        self.secondaries = {}
        pass

    def read(self, file_path):
        with open(file_path, 'rt', 1) as f:
            self.__foreach_line(f)

        self.handler.on_finish()

    def __foreach_line(self, file):
        for line in file:
            self.__trigger_handlers(line)

    def __trigger_handlers(self, line: str):
        prev = self.handler

        if None is not prev:
            prev.handle(line, None)

        for key in self.secondaries:
            self.secondaries[key].handle(content=line,
                                         prev_handler=prev)
            prev = self.secondaries[key]

    def register_handler(self, key: str, abc_reader_handler: ABCReaderHandler):
        self.secondaries[key] = abc_reader_handler

    def get_handler(self, key: str):
        return self.secondaries[key]
