from tsplib.distance import EuclideanDistance2D, EuclideanDistance3D, \
    MaximumDistance2D, MaximumDistance3D, ManhattanDistance2D, \
    ManhattanDistance3D, PseudoEuclideanDistance, GeoGraphicalDistance, \
    MatrixDistance, VertexDataMapper, DictVertexDataMapper
from tsplib.parser import Parser, Spec, NameSpecHandler, \
    CommentSpecHandler, TypeSpecHandler, \
    EdgeWeightTypeSpecHandler, CapacitySpecHandler, DimensionSpecHandler, \
    EdgeWeightFormatSpecHandler, EdgeDataFormatSpecHandler, \
    DisplayDataTypeSpecHandler, NodeCoordTypeSpecHandler, SectionSpec, Section, \
    SectionNodeCoord, EdgeWeightTypeSpec, EdgeWeightFormatSpec, \
    SectionEdgeWeight, Descriptor


class DistanceFunctionFactory:
    """
    creates a DistanceFunction depending on the descriptor
    """
    @staticmethod
    def create(descriptor: Descriptor):

        dim = descriptor.dimension

        if EdgeWeightTypeSpec.EXPLICIT is not descriptor.edge_weight_type:

            mapper = DictVertexDataMapper('vertex')
            return {
                EdgeWeightTypeSpec.EUC_2D: EuclideanDistance2D(
                    mapper=mapper, dimension=dim,
                    vertices=descriptor.vertices),
                EdgeWeightTypeSpec.EUC_3D: EuclideanDistance3D(
                    mapper=mapper, dimension=dim,
                    vertices=descriptor.vertices),
                EdgeWeightTypeSpec.MAX_2D: MaximumDistance2D(
                    mapper=mapper, dimension=dim,
                    vertices=descriptor.vertices),
                EdgeWeightTypeSpec.MAX_3D: MaximumDistance3D(
                    mapper=mapper, dimension=dim,
                    vertices=descriptor.vertices),
                EdgeWeightTypeSpec.MAN_2D: ManhattanDistance2D(
                    mapper=mapper, dimension=dim,
                    vertices=descriptor.vertices),
                EdgeWeightTypeSpec.MAN_3D: ManhattanDistance3D(
                    mapper=mapper, dimension=dim,
                    vertices=descriptor.vertices),
                EdgeWeightTypeSpec.GEO: GeoGraphicalDistance(
                    mapper=mapper, dimension=dim,
                    vertices=descriptor.vertices),
                EdgeWeightTypeSpec.ATT: PseudoEuclideanDistance(
                    mapper=mapper, dimension=dim,
                    vertices=descriptor.vertices),
            }.get(descriptor.edge_weight_type, None)

        elif EdgeWeightFormatSpec.FULL_MATRIX is descriptor.edge_weight_format:
            mapper = VertexDataMapper()

            dd = MatrixDistance(
                mapper=mapper, dimension=dim, vertices=descriptor.vertices)

            return dd

        return None


class ParserFactory:
    """
    creates a tsplib parser
    """
    @staticmethod
    def create(descriptor):
        parser = Parser(descriptor)

        section_spec = ParserFactory.__create_section_spec(descriptor)
        section_node_coord = ParserFactory.__create_section_node_coord(
            descriptor)
        section_edge_weight = ParserFactory.__create_section_edge_weight(
            descriptor)

        # register spec handler as first section handler to be default
        parser.register_section_handler(
            Section.SPEC.name,
            section_spec
        )

        # register data parser for node coordinates
        parser.register_section_handler(
            Section.NODE_COORD_SECTION.name,
            section_node_coord
        )

        parser.register_section_handler(
            Section.EDGE_WEIGHT_SECTION.name,
            section_edge_weight
        )

        return parser

    @staticmethod
    def __create_section_spec(descriptor):

        section_spec = SectionSpec(descriptor=descriptor)
        section_spec.register_spec_handler(
            spec=Spec.NAME,
            handler=NameSpecHandler(descriptor=descriptor))

        section_spec.register_spec_handler(
            spec=Spec.COMMENT,
            handler=CommentSpecHandler(descriptor=descriptor))

        section_spec.register_spec_handler(
            spec=Spec.TYPE,
            handler=TypeSpecHandler(descriptor=descriptor))

        section_spec.register_spec_handler(
            spec=Spec.EDGE_WEIGHT_TYPE,
            handler=EdgeWeightTypeSpecHandler(descriptor=descriptor))

        section_spec.register_spec_handler(
            spec=Spec.DIMENSION,
            handler=DimensionSpecHandler(descriptor=descriptor))

        section_spec.register_spec_handler(
            spec=Spec.CAPACITY,
            handler=CapacitySpecHandler(descriptor=descriptor))

        section_spec.register_spec_handler(
            spec=Spec.EDGE_WEIGHT_FORMAT,
            handler=EdgeWeightFormatSpecHandler(descriptor=descriptor))

        section_spec.register_spec_handler(
            spec=Spec.EDGE_DATA_FORMAT,
            handler=EdgeDataFormatSpecHandler(descriptor=descriptor))

        section_spec.register_spec_handler(
            spec=Spec.DISPLAY_DATA_TYPE,
            handler=DisplayDataTypeSpecHandler(descriptor=descriptor))

        section_spec.register_spec_handler(
            spec=Spec.NODE_COORD_TYPE,
            handler=NodeCoordTypeSpecHandler(descriptor=descriptor))

        return section_spec

    @staticmethod
    def __create_section_node_coord(descriptor):
        return SectionNodeCoord(descriptor=descriptor)

    @staticmethod
    def __create_section_edge_weight(descriptor):
        return SectionEdgeWeight(descriptor=descriptor)
