=============================================================
tsplib 95 parser 
=============================================================

https://www.iwr.uni-heidelberg.de/groups/comopt/software/TSPLIB95/

spec available at /doc/tsp95.pdf


implemented Features: 
this is not a full implementation. implementation only for symmetric tsp and asymmetric tsp

edge weight types (implementation by spec): 
	- euclidean distance 2+3d
	- manhattan distance 2+3d, 
	- maximum distance 2+3d, 
	- geo coordinates, 
	- pseudo euclidean 
	
edge weight format: 
	- function
	- full matrix

=============================================================
TSP algorithms
=============================================================

algorithms implementations:
	- simulated annealing
	- hill climbing


mutation operator implementation
	- 2-Opt
	- OrOpt
	- Shifting
	- Two Exchange
	- B.R. Heap brute force permutation


=============================================================
Usage: look at test cases: 
	- tsplib/tests/test_tsplib.py
	- tsplib/tests/algorithm/test_simulated_annealing.py
	- tsplib/tests/algorithm/test_hill_climbing.py

Sample files are available at /assets


this project requires python 3.6